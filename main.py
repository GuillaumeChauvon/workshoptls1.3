from math import *
import random
import time
#Bitcoin secp256k1

#expression : y^2 = x^3+7

xb = 0x79BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798
yb = 0x483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8
pb = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F
#pb = 46260047
#xb = 25716408
#yb = 5891503
#secp256r1



#expression : y^2 = x^3 -x*3+b

x = 0x6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296
y = 0x4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5
b = 0x5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b
psecp = 115792089210356248762697446949407573530086143415290314195533631308867097853951




#random.seed(420200)
KeyC = random.randint(1, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F)
#KeyC = 818260462
#print("cle")
#print(KeyC)
KeyS = random.randint(1, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F)

def doublement(P, a, p): # Y²Z=X³+aXZ²+bZ³ == Y² = X³ +a X + b
    Xp = P[0]
    Yp = P[1]
    Zp = P[2]
    A = (a * Zp * Zp + 3*Xp*Xp) % p
    B = (Yp * Zp) % p
    C = (Xp * Yp * B) % p
    D = (A * A - 8 *C) %p
    Xp3 = (2*B*D) % p
    Yp3 = (A*(4*C - D) - 8 * Yp * Yp * B *B)%p
    Zp3 = (8 * B * B * B)%p
    return [Xp3, Yp3,Zp3]

 

#print("doublement")
#d = doublement([55066263022277343669578718895168534326250603453777594175500187360389116729240,32670510020758816978083085130507043184471273380659243275938904335757337482424,1], 0, pb)

#print(d[0], d[1], d[2])

#d[0] = d[0]*83896767256196052593227684192059777885330921417918712932395718639823214834020 %pb
#d[1] = d[1]*83896767256196052593227684192059777885330921417918712932395718639823214834020 %pb
#print("affine")
#print(d[0], d[1])



def addition(P, Q, p):
    Xp1 = P[0]
    Yp1 = P[1]
    Zp1 = P[2]
    Xp2 = Q[0]
    Yp2 = Q[1]
    Zp2 = Q[2]
    A = (Yp2 * Zp1 - Yp1 * Zp2)%p
    B = (Xp2 * Zp1 - Xp1 * Zp2)%p
    C = (A *A  * Zp1 * Zp2 - B * B * B - 2 * B * B * Xp1 * Zp2)%p
    Xp3 = (B * C)%p
    Yp3 = (A * (B*B * Xp1 * Zp2 -C) - B*B*B*Yp1*Zp2)%p
    Zp3 = (B*B*B*Zp1*Zp2)%p
    return [Xp3, Yp3, Zp3]

a= addition([39891559044137870870298580324971619466303765610204192757303931466210199710176,44719134668762104847090018939723555404365403238117103035118576159924616976764,1],[53599308067936698932360038523359083228859211492754988097367085446566862431162,37327558048038169042034311262162937507217179662902526332280499684157207681929,1],pb)
a[0]= a[0]*68245843335517345209581772323749036092485499628638915328877155486489218375199%pb
a[1]= a[1]* 68245843335517345209581772323749036092485499628638915328877155486489218375199%pb
#print("add")
#print(a)


def MontgomeryLadder(r, P, a, p) : # r = string of bits P = [X, Y, Z] a= coeff a de la courbe elliptique format weiestrass
    #print(r)
    T1 = P
    T2 = doublement(P, a, p)
    for i in range (1,len(r)):
        if r[i] == '0' :
            T2 = addition(T1, T2, p)
            T1 = doublement(T1 , a, p)
        elif r[i] == '1' :
            T1 = addition(T1, T2, p)
            T2 = doublement(T2, a, p)
        else :
            print("r != 1 ou 0")

    return T1



result = MontgomeryLadder("{0:b}".format(KeyC), [xb,yb,1], 0,pb)
print("Montgomery")
print(result)


def checkResult(P, a ,b):
    X = P[0]
    Y = P[1]
    Z = P[2]
    if Y*Y*Z % pb  == (X*X*X+ a*X*Z*Z + b*Z*Z*Z) % pb:
        print("success")
    else : 
        print("fail")
    return

checkResult(result, 0, 7)

somme = 0
for i in range (10000): 
    T_deb = time.clock()
    Key = random.randint(1, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F)
    result = MontgomeryLadder("{0:b}".format(Key), [xb,yb,1], 0,pb)
    checkResult(result, 0, 7)
    T_fin = time.clock()
    print(i)
    tps = T_fin - T_deb
    somme += tps
    #print(T_fin - T_deb)

print(somme/10000)
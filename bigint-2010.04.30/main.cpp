#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "BigIntegerLibrary.hh"
#include <string>
#include <vector>
#include <iostream>

struct PointPr
{
    BigInteger X;
    BigInteger Y;
    BigInteger Z;
};


struct ParamCurve
{
    BigInteger a;
    BigInteger b;
    BigInteger p;
    BigInteger r;
    BigInteger r2modp;
    BigInteger v;
    BigInteger g;
    BigInteger rm1;
};

class MyBigInteger
{
    public :
    MyBigInteger(){
        data.resize(8,0); 
        sign = 0;
    }
    MyBigInteger(int a){
        data.resize(8,0);  
        if (a > 0){sign = 1;}
        else if (a ==0) {sign = 0;}
        else {sign = -1;}
        data[0] = (uint32_t)a;
    }
    MyBigInteger(std::string s){
        std::vector<uint8_t> decimalform;
        data.resize(8,0);
        int i = 0;
        if(s[0] == '-'){sign = -1; i = 1;}
        else if (s == "0"){sign = 0;}
        else{sign = 1;}
        for (i; i < s.length(); i++){
            decimalform.push_back((uint8_t)s[i]-48);
        }

    }
    ~MyBigInteger(){}
    private :
    std::vector<uint32_t> data;
    int sign;
};





BigInteger MM(BigInteger A, BigInteger B, BigInteger r, BigInteger v, BigInteger n) // Montgomery Multiply
{
    BigInteger s = A*B;
    BigInteger t = (s*v) % r;
    BigInteger m = s+t*n;
    //std::cout << "m : " << m << "; s = " << s << "; t = " << t << "; n = " << n << std::endl;
    BigInteger u = m/r;
    //std::cout << std::endl << "U : " << u << std::endl;
    if (u >= n)
    {
        //std::cout << "u>n" << std::endl;
        return u-n;
    }
    else
    {
        return u;
    }
} 

PointPr PassageMontgomery(PointPr P, ParamCurve C)
{
    BigInteger Rx = MM(P.X, C.r2modp, C.r, BigInteger(-1) * C.v, C.p);
    BigInteger Ry = MM(P.Y, C.r2modp, C.r, BigInteger(-1) * C.v, C.p);
    BigInteger Rz = MM(P.Z, C.r2modp, C.r, BigInteger(-1) * C.v, C.p);
    PointPr R;
    R.X = Rx; R.Y = Ry; R.Z = Rz;
    return R;

}

BigInteger PassageMontgomery(BigInteger P, ParamCurve C)
{
    
    return  MM(P, C.r2modp, C.r, BigInteger(-1) * C.v, C.p);

}

PointPr PassageMontgomeryInverse(PointPr P, ParamCurve C)
{
    BigInteger Rx = MM(P.X, 1, C.r, BigInteger(-1) * C.v, C.p);
    BigInteger Ry = MM(P.Y, 1, C.r, BigInteger(-1) * C.v, C.p);
    BigInteger Rz = MM(P.Z, 1, C.r, BigInteger(-1) * C.v, C.p);
    PointPr R;
    R.X = Rx, R.Y = Ry; R.Z = Rz;
    return R;
}

PointPr Doublement(PointPr P, ParamCurve Cur)
{
    BigInteger Param2(2);
    BigInteger Param3(3);
    BigInteger Param4(4);
    BigInteger Param8(8);
    PointPr Double;
    BigInteger X = P.X;
    BigInteger Y = P.Y;
    BigInteger Z = P.Z;
    BigInteger A = (Cur.a * Z*Z + Param3 *X*X) % Cur.p;
    BigInteger B = (Y * Z) % Cur.p;
    BigInteger C = (X * Y * B) % Cur.p;
    BigInteger D = (A*A - Param8 * C) % Cur.p;
    Double.X = ((BigInteger)2 * B * D) % Cur.p;
    Double.Y = (A * (Param4 * C - D) - Param8 * Y*Y * B*B) % Cur.p;
    Double.Z = (Param8 * B * B * B) % Cur.p;
    return Double;
}

PointPr DoublementModulaire(PointPr P, ParamCurve Cur, BigInteger mtg2, BigInteger mtg3, BigInteger mtg4, BigInteger mtg8, BigInteger amtg)
{

    PointPr Double;
    BigInteger X = P.X;
    BigInteger Y = P.Y;
    BigInteger Z = P.Z;
    //BigInteger A = (Cur.a * Z*Z + Param3 *X*X) % p;
    BigInteger A = MM(MM(amtg, Z, Cur.r , BigInteger(-1) * Cur.v, Cur.p), Z, Cur.r , BigInteger(-1) * Cur.v, Cur.p) + MM(MM(mtg3, X, Cur.r , BigInteger(-1) * Cur.v, Cur.p), X, Cur.r , BigInteger(-1) * Cur.v, Cur.p);
    //BigInteger B = (Y * Z) % Cur.p;
    BigInteger B = MM(Y, Z, Cur.r , BigInteger(-1) * Cur.v, Cur.p);
    //BigInteger C = (X * Y * B) % Cur.p;
    BigInteger C = MM(MM(X, Y, Cur.r , BigInteger(-1) * Cur.v, Cur.p), B, Cur.r , BigInteger(-1) * Cur.v, Cur.p);
    //BigInteger D = (A*A - Param8 * C) % Cur.p;
    BigInteger D = MM(A, A, Cur.r , BigInteger(-1) * Cur.v, Cur.p) - MM(mtg8, C, Cur.r , BigInteger(-1) * Cur.v, Cur.p);
    //Double.X = ((BigInteger)2 * B * D) % Cur.p;
    Double.X = MM(MM(mtg2, B, Cur.r , BigInteger(-1) * Cur.v, Cur.p), D, Cur.r , BigInteger(-1) * Cur.v, Cur.p);
    //Double.Y = (A * (Param4 * C - D) - Param8 * Y*Y * B*B) % Cur.p;
    Double.Y = MM(A, (MM(mtg4, C, Cur.r , BigInteger(-1) * Cur.v, Cur.p)-D), Cur.r , BigInteger(-1) * Cur.v, Cur.p) - MM(MM(MM(MM(mtg8, Y, Cur.r , BigInteger(-1) * Cur.v, Cur.p), Y, Cur.r , BigInteger(-1) * Cur.v, Cur.p), B, Cur.r , BigInteger(-1) * Cur.v, Cur.p), B, Cur.r, BigInteger(-1) * Cur.v, Cur.p);
    //Double.Z = (Param8 * B * B * B) % Cur.p;
    Double.Z = MM(MM(MM(mtg8, B, Cur.r , BigInteger(-1) * Cur.v, Cur.p), B, Cur.r , BigInteger(-1) * Cur.v, Cur.p), B, Cur.r , BigInteger(-1) * Cur.v, Cur.p);
    return Double;
}

PointPr Addition(PointPr P, PointPr Q, ParamCurve Cur)
{
    BigInteger Param2(2);
    BigInteger A = (Q.Y * P.Z - P.Y * Q.Z) % Cur.p;
    BigInteger B = (Q.X * P.Z - P.X * Q.Z) % Cur.p;
    BigInteger C = (A*A * P.Z * Q.Z  - B*B*B - Param2 * B*B *P.X*Q.Z)%Cur.p;
    PointPr Add;
    Add.X = ( B*C)%Cur.p;
    Add.Y = (A*(B*B*P.X*Q.Z -C)- B*B*B*P.Y*Q.Z)%Cur.p;
    Add.Z = (B*B*B*P.Z*Q.Z)%Cur.p;
    return Add;
}

PointPr AdditionModulaire(PointPr P, PointPr Q, ParamCurve Cur, BigInteger mtg2)
{

    //BigInteger A = (Q.Y * P.Z - P.Y * Q.Z) % Cur.p;
    BigInteger A = MM(Q.Y,P.Z,Cur.r, BigInteger(-1)*Cur.v, Cur.p)-MM(P.Y,Q.Z,Cur.r, BigInteger(-1)*Cur.v, Cur.p);
    //BigInteger B = (Q.X * P.Z - P.X * Q.Z) % Cur.p;
    BigInteger B = MM(Q.X,P.Z,Cur.r, BigInteger(-1)*Cur.v, Cur.p)-MM(P.X,Q.Z,Cur.r, BigInteger(-1)*Cur.v, Cur.p);
    //BigInteger C = (A*A * P.Z * Q.Z  - B*B*B - Param2 * B*B *P.X*Q.Z)%Cur.p;
    BigInteger C = MM(A,MM(A,MM(P.Z,Q.Z,Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p) - MM(B,MM(B,B,Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p) - MM(mtg2,MM(B,MM(B,MM(P.X,Q.Z,Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p);
    PointPr Add;
    //Add.X = ( B*C)%Cur.p;
    Add.X = MM(B,C,Cur.r, BigInteger(-1)*Cur.v, Cur.p);
    //Add.Y = (A*(B*B*P.X*Q.Z -C)- B*B*B*P.Y*Q.Z)%Cur.p;
    Add.Y = MM(A,(MM(B,MM(B,MM(P.X,Q.Z,Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p)-C),Cur.r, BigInteger(-1)*Cur.v, Cur.p) - MM(B,MM(B,MM(B,MM(P.Y,Q.Z,Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p);
    //Add.Z = (B*B*B*P.Z*Q.Z)%Cur.p;
    Add.Z = MM(B,MM(B,MM(B,MM(P.Z,Q.Z,Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r, BigInteger(-1)*Cur.v, Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p),Cur.r,BigInteger(-1)*Cur.v,Cur.p);
    return Add;
}

PointPr MontgomeryLadder(std::string r, PointPr P, ParamCurve Cur)
{

    PointPr T1 = P;
    PointPr T2 = Doublement(P, Cur);
    for (int i = 1; i < r.length();i++)
    {
        if (r[i] == '0')
        {
            T2 = Addition(T1, T2, Cur);
            T1 = Doublement(T1, Cur);
        }
        else if (r[i]== '1')
        {
            T1 = Addition(T1, T2, Cur);
            T2 = Doublement(T2, Cur);
        }
    }
    return T1;
}



PointPr MontgomeryLadderModulaire(std::string r, PointPr P, ParamCurve Cur, BigInteger Param2, BigInteger Param3, BigInteger Param4, BigInteger Param8, BigInteger Parama)
{
    PointPr T1 = PassageMontgomery(P,Cur);
    PointPr T2 = DoublementModulaire(T1, Cur, Param2, Param3, Param4, Param8, Parama);
    for (int i = 1; i < r.length();i++)
    {
        if (r[i] == '0')
        {
            T2 = AdditionModulaire(T1, T2, Cur, Param2);
            T1 = DoublementModulaire(T1, Cur, Param2, Param3, Param4, Param8, Parama);
        }
        else if (r[i]== '1')
        {
            T1 = AdditionModulaire(T1, T2, Cur, Param2);
            T2 = DoublementModulaire(T2, Cur, Param2, Param3, Param4, Param8, Parama);
        }
    }
    T1 = PassageMontgomeryInverse(T1, Cur);
    return T1;
}


void CheckResult(PointPr P, ParamCurve Cur)
{
    if (P.Y*P.Y*P.Z % Cur.p == (P.X*P.X*P.X + Cur.a*P.X*P.Z*P.Z + Cur.b*P.Z*P.Z*P.Z) % Cur.p)
    {
        std::cout << "Success" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Failed" << std::endl << std::endl;
    }
}

std::string BigIntegertoBinary(BigInteger n)
{
    std::string r;
    while (n!=0)
    {
        r=(n%2==0 ? "0" : "1")+r; n/=2;
    }
    return r;
}




int main()
{

    //init 
    ParamCurve BitcoinCurve;
    BitcoinCurve.a = 0;
    BitcoinCurve.b = 7;
    std::string sP = "115792089237316195423570985008687907853269984665640564039457584007908834671663";
    BitcoinCurve.p = stringToBigInteger(sP);
    PointPr BitcoinPoint;
    std::string sX = "55066263022277343669578718895168534326250603453777594175500187360389116729240";
    std::string sY = "32670510020758816978083085130507043184471273380659243275938904335757337482424";
    BitcoinPoint.X = stringToBigInteger(sX);
    BitcoinPoint.Y = stringToBigInteger(sY);
    BitcoinPoint.Z = 1;
    
    
    BigInteger r = 2;
    for (int i = 0; i < 255; i++){r *= 2;}
    BitcoinCurve.r =r;

    

    extendedEuclidean(BitcoinCurve.r,BitcoinCurve.p,BitcoinCurve.g,BitcoinCurve.rm1,BitcoinCurve.v);
    /*std::cout<< "n :" << BitcoinCurve.p << std::endl;
    std::cout<< "r :" << BitcoinCurve.r << std::endl;
    std::cout<< "rm1 :" << BitcoinCurve.rm1 << std::endl;
    std::cout<< "-v :" << BitcoinCurve.v << std::endl;
    std::cout<< "g :" << BitcoinCurve.g << std::endl;*/
    BitcoinCurve.r2modp = (BitcoinCurve.r * BitcoinCurve.r) %BitcoinCurve.p;

    BigInteger Param2 = PassageMontgomery(BigInteger(2), BitcoinCurve);
    BigInteger Param3 = PassageMontgomery(BigInteger(3), BitcoinCurve);
    BigInteger Param4 = PassageMontgomery(BigInteger(4), BitcoinCurve);
    BigInteger Param8 = PassageMontgomery(BigInteger(8), BitcoinCurve);
    BigInteger Parama = PassageMontgomery(BitcoinCurve.a, BitcoinCurve);

    double somme_modulaire = 0;
    double somme_non_modulaire = 0;
    srand(time(NULL));
    for (int i = 0; i< 1000; i++)
    {
    std::cout << i << std::endl;
    float key = rand()/(float)RAND_MAX;
    BigInteger keyB = (BigInteger)((int)(key * 1000000 + 0.5)) * BitcoinCurve.p / 1000000;
    keyB = rand();
    std::cout << "clé : " << keyB <<std::endl;
    std::string pBinary = BigIntegertoBinary(keyB);
    //std::cout <<pBinary<< std::endl;

    double t_deb_mod = clock();
    std::cout << "Modulaire : " << std::endl;
    PointPr d = MontgomeryLadderModulaire(pBinary, BitcoinPoint, BitcoinCurve, Param2, Param3, Param4, Param8, Parama);
    //std::cout<< "dX : "<<d.X<<"  dY : "<<d.Y<<"  dZ : "<< d.Z<< std::endl;
    CheckResult(d, BitcoinCurve);
    double t_fin_mod = clock();
    //double t_deb = clock();
    //std::cout << "Non Modulaire : " << std::endl;
    //PointPr d = MontgomeryLadder(pBinary, BitcoinPoint, BitcoinCurve);
    //std::cout<< "dX : "<<d.X<<"  dY : "<<d.Y<<"  dZ : "<< d.Z<< std::endl;
    //CheckResult(d, BitcoinCurve);
    //double t_fin = clock();
    somme_modulaire += ((double)t_fin_mod-t_deb_mod)/ CLOCKS_PER_SEC;
    }

    std::cout<< "moyenne mod : " << somme_modulaire/1000 << std::endl;
    std::cout<< "moyenne non mod : " << somme_non_modulaire/1000 << std::endl;

    // Montgomery Multiplication
    //std::cout << "Montgomery Multiply" << std::endl << std::endl;


    
    // Test MontgomeryMultiply
    /*BigInteger A = BitcoinPoint.X;
    BigInteger Amtg = MM(A, BitcoinCurve.r2modp, BitcoinCurve.r , BigInteger(-1) * BitcoinCurve.v, BitcoinCurve.p);
    BigInteger B = BitcoinPoint.Y;
    BigInteger Bmtg = MM(B, BitcoinCurve.r2modp, BitcoinCurve.r ,BigInteger(-1) * BitcoinCurve.v, BitcoinCurve.p);

    BigInteger resultmtg = MM(Amtg, Bmtg, BitcoinCurve.r, BigInteger(-1) * BitcoinCurve.v, BitcoinCurve.p);
    BigInteger result = MM(resultmtg, 1, BitcoinCurve.r, BigInteger(-1) * BitcoinCurve.v, BitcoinCurve.p);
    std::cout << "produit A*B modulo p avec MM: " << result << std::endl;
    BigInteger result2 = (A * B) % BitcoinCurve.p;
    std::cout << "produit A*B modulo p sans MM: " << result2 << std::endl;*/

    // Test Doublement et addition modulaire 
    /*BigInteger Param2 = PassageMontgomery(BigInteger(2), BitcoinCurve);
    BigInteger Param3 = PassageMontgomery(BigInteger(3), BitcoinCurve);
    BigInteger Param4 = PassageMontgomery(BigInteger(4), BitcoinCurve);
    BigInteger Param8 = PassageMontgomery(BigInteger(8), BitcoinCurve);
    BigInteger Parama = PassageMontgomery(BitcoinCurve.a, BitcoinCurve);
    PointPr Bitcoinmtg = PassageMontgomery(BitcoinPoint, BitcoinCurve);
    PointPr result = DoublementModulaire(Bitcoinmtg, BitcoinCurve,Param2, Param3, Param4, Param8, Parama );
    PointPr AddPointDebug = result;
    result = PassageMontgomeryInverse(result, BitcoinCurve);
    std::cout << " DEBUG DOUBLEMENT" << std::endl;
    std::cout<< "doublement modulaire : "<< std::endl<< "X : "<<result.X<<"  Y : "<<result.Y<<"  Z : "<< result.Z<< std::endl;
    result = Doublement(BitcoinPoint, BitcoinCurve);
    std::cout<< "doublement non modulaire : "<<std::endl<< "X : "<<result.X<<"  Y : "<<result.Y<<"  Z : "<< result.Z<< std::endl;
    
    
    std::cout << std::endl << "DEBUG ADDITION" << std::endl;
    result = AdditionModulaire(Bitcoinmtg, AddPointDebug, BitcoinCurve, Param2);
    result = PassageMontgomeryInverse(result, BitcoinCurve);
    std::cout<< "addition modulaire : "<< std::endl<< "X : "<<result.X<<"  Y : "<<result.Y<<"  Z : "<< result.Z<< std::endl;
    AddPointDebug = PassageMontgomeryInverse(AddPointDebug, BitcoinCurve);
    result = Addition(BitcoinPoint, AddPointDebug, BitcoinCurve);
    std::cout<< "addition non modulaire : "<< std::endl<< "X : "<<result.X<<"  Y : "<<result.Y<<"  Z : "<< result.Z<< std::endl;*/

    //MyBigInteger a("115792089237316195423570985008687907853269984665640564039457584007908834671663");

    return 0;
}